

smart-door-bell controll panel frontend & backend application

## Getting Started

```sh
$ npm install
```

Start the local dev server:

```sh
$ npm run server
```

Navigate to **http://localhost:8080/** to view the app.


### build

```sh
$ npm run build
```

Build minified app for production using the [production](http://webpack.github.io/docs/cli.html#production-shortcut-p) shortcut.

### test

```sh
$ npm test
```


### clean

```sh
$ npm run clean
```

**Input:** `build/app.js`

Removes the compiled app file from build.

