import React, { Component } from "react";
import { browserHistory } from "react-router";
import styles from "./style.css";
import "./react-bootstrap-table.css";
import ReactTable from "react-table";
import "react-table/react-table.css";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import _ from "lodash";

// Simulates the call to the server to get the data
const fakeDataFetcher = {
  fetch(page, size) {
    return new Promise((resolve, reject) => {
      fetch("/getImages")
        .then(function(response) {
          const jsonImages = response.json().then(jsonImagesParsed => {
            fetch("/getVideos").then(response => {
              const jsonVideos = response.json().then(jsonVideosParsed => {
                resolve(
                  _.sortBy(
                    _.concat(
                      jsonImagesParsed.resources,
                      jsonVideosParsed.resources
                    ),
                    "created_at"
                  )
                );
              });
              // resolve(jsonVideos.concat(jsonImages));
            });
          });
        })
        .catch(function(err) {
          console.log(err);
        });
    });
  }
};

function linkFormatter(cell, row) {
  return `<a href="${cell}" target="_blank">${cell}</a>`;
}

function dateFormatter(cell, row) {
  const newDate = new Date(cell);
  return (
    newDate.getFullYear() +
    "-" +
    (newDate.getMonth() + 1) +
    "-" +
    newDate.getDate() +
    " " +
    newDate.getHours() +
    ":" +
    newDate.getMinutes() +
    ":" +
    newDate.getSeconds()
  );
}

function actionButtonFormatter(cell, row) {
  console.log(row);
  const currentDate = new Date();
  const eventDate = new Date(row.created_at);
  if (currentDate.getTime() - eventDate.getTime() > 300000) {
    return `<a href="https://meet.jit.si/DoorBell" class="btn btn-info" target="_blank">Join A call!</a>`;
  }
}



export default class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [],
      totalSize: 0,
      page: 1,
      sizePerPage: 20
    };
    this.fetchData = this.fetchData.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleSizePerPageChange = this.handleSizePerPageChange.bind(this);

    this.handleLockClick = this.handleLockClick.bind(this);
    this.handleUnlockClick = this.handleUnlockClick.bind(this);

  }

  handleLockClick() {
    let headers = new Headers();
    headers.append('Authorization', 'Basic YWRtaW46YWRtaW4=');
    fetch('/setLocked', {headers: headers})
  }

  handleUnlockClick() {
    let headers = new Headers();
    headers.append('Authorization', 'Basic YWRtaW46YWRtaW4=');
    fetch('/setUnlocked', {headers: headers})

  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData(page = this.state.page, sizePerPage = this.state.sizePerPage) {
    fakeDataFetcher.fetch(page, sizePerPage).then(data => {
      this.setState({ items: data, totalSize: 10, page, sizePerPage });
    });
  }

  handlePageChange(page, sizePerPage) {
    this.fetchData(page, sizePerPage);
  }

  handleSizePerPageChange(sizePerPage) {
    // When changing the size per page always navigating to the first page
    this.fetchData(1, sizePerPage);
  }

  render() {
    const options = {
      onPageChange: this.handlePageChange,
      onSizePerPageList: this.handleSizePerPageChange,
      page: this.state.page,
      sizePerPage: this.state.sizePerPage
    };

    return (
      <div className="tableContainer">
      <div className="toolbar" style={{'display':'flex', 'justifyContent':'space-between'}}>
        <div className="actionButtons">
          <a className="btn btn-info" onClick={this.handleLockClick}>Lock</a>
          <a className="btn btn-danger" onClick={this.handleUnlockClick}>Unlock</a>
        </div>
        <a href="https://meet.jit.si/DoorBell" className="btn btn-info" target="_blank">Join A call!</a>
      </div>
        
        <BootstrapTable
          data={this.state.items}
          options={options}
          fetchInfo={{ dataTotalSize: this.state.totalSize }}
          remote
          striped
          hover
          condensed
        >
          <TableHeaderColumn
            dataField="created_at"
            isKey
            dataAlign="left"
            dataFormat={dateFormatter}
          >
            Action Time
          </TableHeaderColumn>
          <TableHeaderColumn
            dataField="secure_url"
            dataAlign="center"
            dataFormat={linkFormatter}
          >
            Resource Link
          </TableHeaderColumn>
          <TableHeaderColumn dataField="resource_type" dataAlign="center">
            Action Type
          </TableHeaderColumn>
        </BootstrapTable>
        </div>
    );
  }
}
