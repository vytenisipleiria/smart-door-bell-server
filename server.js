import express from "express";
const app = express();
// var basicAuth = require('express-basic-auth')
var basicAuth = require('express-basic-auth')
var request = require("request");
var https = require('https');
var fs = require('fs');



//Requires basic auth with username 'Admin' and password 'secret1234'
var staticUserAuth = basicAuth({
  users: {
      'admin': 'admin'
  },
  challenge: false
})


/************************************************************
 *
 * Express routes for:
 *   - app.js
 *   - style.css
 *   - index.html
 *
 ************************************************************/

// Serve application file depending on environment

let lockStatus = true;

app.get("/setUnlocked", staticUserAuth, (req, res) => {
  lockStatus = false;
  // res.sendStatus(200)

  res.json({status: "Unlocked succesfully"})
})

app.get("/setLocked", staticUserAuth, (req, res) => {
  lockStatus = true;
  // res.sendStatus(200)

  res.json({status: "Locked succesfully"})
})

app.get("/getLockStatus", (req, res) => {
  res.json(lockStatus);
}) 

app.get("/app.js", (req, res) => {
  //if (process.env.PRODUCTION) {
    res.sendFile(__dirname + "/build/app.js");
    // res.redirect("//localhost:9090/build/app.js");
    // res.redirect("http://localhost:9090/build/app.js");
    
  // }
  //  else {
  //   res.redirect("//localhost:9090/build/app.js");
  // }
});

// Serve aggregate stylesheet depending on environment
app.get("/style.css", (req, res) => {
  //if (process.env.PRODUCTION) {
    res.sendFile(__dirname + "/build/style.css");
    // res.redirect("//localhost:9090/build/style.css");
    // res.redirect("http://localhost:9090/build/style.css");

  // } 
  // else {
  //   res.redirect("//localhost:9090/build/style.css");
  // }
});

app.get("/getImages", (req, res) => {
  const imageUrl =
    "https://981161382723683:CKOUBz0kKcgnaCZv9gFSiurW_uY@api.cloudinary.com/v1_1/dyi8u4qt7/resources/image";
  request.get(imageUrl, (error, response, body) => {
    let jsonImages = JSON.parse(body);
    console.log(jsonImages);

    res.json(jsonImages);
  });
});

app.get("/getVideos", (req, res) => {
  const videoUrl =
  "https://981161382723683:CKOUBz0kKcgnaCZv9gFSiurW_uY@api.cloudinary.com/v1_1/dyi8u4qt7/resources/video";

  request.get(videoUrl, (error, response, body) => {
    let jsonVideos = JSON.parse(body);
    console.log(jsonVideos);

    res.json(jsonVideos)
  })
})


// Serve index page
app.get("*", (req, res) => {
  res.sendFile(__dirname + "/build/index.html");
});

/*************************************************************
 *
 * Webpack Dev Server
 *
 * See: http://webpack.github.io/docs/webpack-dev-server.html
 *
 *************************************************************/

if (!process.env.PRODUCTION) {
  const webpack = require("webpack");
  const WebpackDevServer = require("webpack-dev-server");
  const config = require("./webpack.local.config");

  new WebpackDevServer(webpack(config), {
    publicPath: config.output.publicPath,
    hot: true,
    noInfo: true,
    historyApiFallback: true
  }).listen(9090, "localhost", (err, result) => {
    if (err) {
      console.log(err);
    }
  });
}

/******************
 *
 * Express server
 *
 *****************/

const port = process.env.PORT || 8080;

var options = {
  key: fs.readFileSync( './localhost.key' ),
  cert: fs.readFileSync( './localhost.cert' ),
  requestCert: false,
  rejectUnauthorized: false
};

// var server = https.createServer( options, app );

// server.listen( port, function () {
//   console.log( 'Express server listening on port ' + server.address().port );
// } );

const server = app.listen(port, () => {
  const host = server.address().address;
  const port = server.address().port;

  console.log("Essential React listening at http://%s:%s", host, port);
});
